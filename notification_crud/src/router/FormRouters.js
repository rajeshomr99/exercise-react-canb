import { ExampleForm } from "../components/userCreateForm/exampleform";
import { UserDetail } from "../components/userDetailList/UserDetail";
import { FormPath, Home } from "./routerPath";

export const formRouter = [
    {
      path: Home,
      exact: true,
      component: UserDetail,
    },
    {
      path: FormPath,
      exact: false ,
      component: ExampleForm,
    },
  ];
  