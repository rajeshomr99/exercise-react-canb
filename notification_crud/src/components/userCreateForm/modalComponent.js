import { Box, Button, Modal, Stack, TextField } from "@mui/material";
import "./modalcomponent.scss";
import { MultiVale } from "../commomComponents/multileValueComponent";
import { TextFieldComponent } from "../commomComponents/texfield";
import { offer, pincode, plansType, professions } from "../contants";
import {
  checkBox,
  CheckBoxComponent,
} from "../commomComponents/CheckBoxComponent";
import { useEffect, useState } from "react";

export const ModalComponent = (props) => {
  const { handleClose, setData, data, open } = props;
  const [userData, setUserData] = useState(data);
  useEffect(() => {
    setUserData(data);
  }, [data]);
  const handleCheck = () => {
    setUserData(data);
    handleClose();
  };
  const [errrormsg, setErrormsg] = useState({
    agelimit: "",
    ageTo: "",
    pincode: "",
    professions: "",
    plans: "",
    promotion: "",
  });
  const userTypes = [
    { id: 1, name: "Individual" },
    { id: 2, name: "Team" },
    { id: 3, name: "Company" },
  ];
  const gender = [
    { id: 1, name: "Male" },
    { id: 2, name: "Female" },
  ];

  const [errorshow, seterror] = useState(false);
  const handleChange = (e, name) => {
    setUserData({ ...userData, [name]: e.target.value });
    setErrormsg({ ...errrormsg, [name]: "" });
  };
  const setcheckedData = (value, name) => {
    //  console.log(value,name);
    if (userData[name].some((sc) => sc === value)) {
      let index = userData[name].findIndex((sc) => sc === value);
      userData[name].splice(index, 1);
    } else {
      userData[name].push(value);
    }
    setUserData({ ...userData });
  };
  const addmutiplevalue = (event, newValue, name) => {
    setUserData({ ...userData, [name]: [...newValue] });
    setErrormsg({ ...errrormsg, [name]: "  " });
  };
  const submitForm = () => {
    let temp = errrormsg;
    let checkvalue = true;
    seterror(true);
    for (const property in userData) {
      //console.log(Number(userData[property]))
      if (
        (!userData[property] && property !== "valid") ||
        userData[property].length === 0
      ) {
        checkvalue = false;
        temp[property] = "Enter the vaild value";
      }
      if (property === "agelimit" || property === "ageTo") {
        if (Number(userData[property]) <= 0) {
          checkvalue = false;
          temp[property] = "Enter the vaild value";
        }
        if (Number(userData.agelimit) > Number(userData.ageTo)) {
          checkvalue = false;
          temp.agelimit = "Enter the value value";
          temp.ageto = "Enter the vaild value";
        }
      }
    }
    setErrormsg({ ...temp });
    if (checkvalue) {
      setData(userData);
      handleCheck();
    }
  };
  console.log(userData);
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "50%",
    height: "400px",
    bgcolor: "white",
    border: "2px solid #000",
    boxShadow: 24,
  };
  return (
    <>
      <Modal
        open={open}
        onClose={handleCheck}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style} component="div" id="modal">
          <div className="modalHead">
            <div className="modalTitle">Application to</div>
            <div className="cancel" onClick={handleCheck}>
              <i class="fas fa-times"></i>
            </div>
          </div>
          <div className="modalContent">
            <div className="ageLimit">
              <TextFieldComponent
                type="number"
                errorMsg={userData.agelimit}
                errorcontent={errrormsg.agelimit}
                errorshow={errorshow}
                handleChange={handleChange}
                name="agelimit"
                label="Age Limit From"
              />
              <TextFieldComponent
                type="number"
                errorMsg={userData.ageTo}
                errorcontent={errrormsg.ageTo}
                errorshow={errorshow}
                handleChange={handleChange}
                name="ageTo"
                label="Age Limit to"
              />
            </div>
            <div className="pincode">
              <Stack spacing={3}>
                <MultiVale
                  label="pincode"
                  errorMsg={userData.pincode}
                  errorcontent={errrormsg.pincode}
                  errorshow={errorshow}
                  value={userData.pincode}
                  addmutiplevalue={addmutiplevalue}
                  optionList={pincode}
                />
              </Stack>
            </div>
            <div className="checkBox">
              <div className="usertype">
                <div>UserType</div>
                <div className="field">
                  <CheckBoxComponent
                    name="userType"
                    checkedData={userData.userType}
                    setcheckedData={setcheckedData}
                    content={userTypes}
                  />
                </div>
                {errorshow && userData.userType.length === 0 && (
                  <span className="errormsg">Check any of one field </span>
                )}
              </div>
              <div className="usertype">
                <div>gender</div>
                <div className="field">
                  <CheckBoxComponent
                    name="gender"
                    checkedData={userData.gender}
                    setcheckedData={setcheckedData}
                    content={gender}
                  />
                </div>
                {errorshow && userData.gender.length === 0 && (
                  <span className="errormsg">Check any of one field </span>
                )}
              </div>
            </div>
            <Stack spacing={3}>
              <MultiVale
                label="plans"
                errorMsg={userData.plans}
                errorcontent={errrormsg.plans}
                errorshow={errorshow}
                addmutiplevalue={addmutiplevalue}
                value={userData.plans}
                optionList={plansType}
              />
              <MultiVale
                label="promotion"
                errorMsg={userData.promotion}
                errorcontent={errrormsg.promotion}
                errorshow={errorshow}
                addmutiplevalue={addmutiplevalue}
                value={userData.promotion}
                optionList={offer}
              />

              <MultiVale
                label="professions"
                errorMsg={userData.professions}
                errorcontent={errrormsg.professions}
                errorshow={errorshow}
                addmutiplevalue={addmutiplevalue}
                value={userData.professions}  
                optionList={professions}
              />
            </Stack>
          </div>
          <div class="submitButton">
            <div>
              <Button onClick={submitForm} variant="contained">
                Apply
              </Button>
            </div>
            <div className="cancelButton">
              <Button onClick={handleCheck} variant="text">
                Cancel
              </Button>
            </div>
          </div>
        </Box>
      </Modal>
    </>
  );
};
