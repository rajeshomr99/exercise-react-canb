export const FormHeader=(props)=>{
    const{goBack}=props;
     return(
        
            <div className='heading'>
                <span className="backSymbol" onClick={goBack}> <i class="fas fa-arrow-left"></i></span>
                <span className="headName"><b>New Notification</b></span>
            </div>

     );
 } 