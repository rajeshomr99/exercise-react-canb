import { Button, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";
import "./CreateForm.scss";
import { useEffect, useState } from "react";
import { FormHeader } from "./FormHeader";
import DateAdapter from "@mui/lab/AdapterDateFns";
import { TextFieldComponent } from "../commomComponents/texfield";
import { RadioButtonComponent } from "../commomComponents/radioButton";
import { SelectFieldComponent } from "../commomComponents/selectTag";
import { ModalComponent } from "./modalComponent";
import { useDispatch, useSelector } from "react-redux";
import { Home } from "../../router/routerPath";
import { addDetails, editDetails, updateDetails } from "../../reduxHandler/action/FormAction";
import { DatePicker, LocalizationProvider, TimePicker } from "@mui/lab";
export const ExampleForm = (props) => {
  const dispatch = useDispatch();
    const notificationTypes = [
      { id: 1, name: "Email" },
      { id: 2, name: "SMS" },
      { id: 3, name: "Mobile App" },
    ];
  const copyParameter = [
    { id: 1, name: "CC" },
    { id: 2, name: "BCC" },
  ];
  const ProductList = [
    { id: 1, name: "amazon" },
    { id: 2, name: "flipkart" },
  ];
  const [userData, setUserData] = useState({
    productName: "",
    date: null,
    frequencytime: null,
    time: null,
    frequency: "",
    increamentTime: "",
    content: "",
    id:Math.random(),
    data: [{agelimit: null,ageTo:null,valid:false,userType:[],gender:[],pincode:[],professions:[],plans:[],promotion:[]}],
    subject: "",
    notificsationType: "",
    copy: "",
  });
  const [errrormsg, setErrormsg] = useState({
    productName: "",
    frequency: "",
    frequencytime: "",
    date: "",
    time: "",
    increamentTime: "",
    content: "",
    subject: "",
    notificsationType: "",
    copy: "",
  });
  const [errorshow, seterror] = useState(false);
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () =>{
   setOpen(false)
  };
  const formvalue = useSelector(state => state);
  useEffect(() => { 
    if(formvalue.index > -1){
      let index=formvalue.userDetails.findIndex(sc=>sc.id===formvalue.index)
      setUserData(formvalue.userDetails[index])
     
    }
  }, [])
  const handleChange = (e, name) => {
    
    setUserData({ ...userData, [name]: e.target.value });
    setErrormsg({ ...errrormsg, [name]: "" });
  };
  const setfields = (value, name) => {
    setUserData({ ...userData, [name]:value== "Invalid Date"? "":value });
        setErrormsg({ ...errrormsg, [name]: "" });
  };
  const setModalData = (value) => {
    
    value.valid=true;
    setUserData({ ...userData, data: [value] });
  };
  const submitForm = () => {
    let temp = errrormsg;
    let checkvalue = true;
    seterror(true);
    for (const property in userData) {
      if (!userData[property] || Number(userData[property]) <= 0 ) {
        checkvalue = false;
        temp[property] = "Enter the vaild value";
      }
      if (property === "subject" || property === "content") {
        if (!userData[property].match(/^[a-zA-Z]+$/)) {
          checkvalue = false;
          temp[property] = "Enter the vaild value";
        }
      }
      if(property==="data" && !userData.data[0].valid){
        checkvalue=false;
        
      }
    }
    setErrormsg({ ...temp });
    if (checkvalue) {
      if(formvalue.index > -1){
        let index=formvalue.userDetails.findIndex(sc=>sc.id===formvalue.index)
        formvalue.userDetails.splice(index,1,userData);
        dispatch(updateDetails(formvalue.userDetails))
      }
      else{
        dispatch(addDetails(userData));
      }
      
      props.history.push(Home);
    }
  };
    const goBack = () => {
      dispatch(editDetails(-1))
      props.history.push(Home);
    };
  
  return (
    <div className="createUser">
      <FormHeader goBack={goBack} />
      <Box
        component="form"
        id="form"
        sx={{ display: "flex", backgroundColor: "#e8e5e5" }}
        noValidate
        autoComplete="on"
      > 
        <div className="formContents">
          <RadioButtonComponent
            errorMsg={userData.notificsationType}
            errorcontent={errrormsg.notificsationType}
            errorshow={errorshow}
            handleChange={handleChange}
            name={"notificsationType"}
            title="Notification Type"
            types={notificationTypes}
          />
          <SelectFieldComponent
          options={ProductList}
            errorMsg={userData.productName}
            errorcontent={errrormsg.productName}
            errorshow={errorshow}
            handleChange={handleChange}
          />
          <TextFieldComponent
            errorMsg={userData.subject}
            errorshow={errorshow}
            errorcontent={errrormsg.subject}  
            handleChange={handleChange}
              name="subject"
              label="Subject"
          />
          <TextFieldComponent
            errorMsg={userData.content}
            errorshow={errorshow}
            errorcontent={errrormsg.content}
            handleChange={handleChange}
            name="content"
            label="Content"
          />
          <Button id="submitForm" onClick={handleOpen}  variant="contained">
            {" "}
            Applicable to
          </Button>
        {(!userData.data[0].valid && errorshow)&&
          <Typography   id="fiedtag" component="div">Enter the fields</Typography>}
          <div className="section2">
            <div className="titeName">Scheduler</div>
            <div className="dateField">
              <LocalizationProvider dateAdapter={DateAdapter}>
                <DatePicker
                  label="Date"
                  value={userData.date}
                  onChange={(newValue) => {
                    setfields(newValue, "date");
                  }}
                  renderInput={(params) => (
                    <TextField
                      sx={{ width: "30%" }}
                      error={true}
                      helperText={errrormsg.date}
                      {...params}
                      variant="filled"
                    />
                  )}
                />
                <TimePicker
                  label="time"
                  value={userData.time}
                  onChange={(newValue) => {
                    setfields(newValue, "time");
                  }}
                  renderInput={(params) => (
                    <TextField
                      error
                      sx={{ width: "30%" }}
                      helperText={errrormsg.time}
                      {...params}
                      variant="filled"
                    />
                  )}
                />
                <TimePicker
                  label="Frequency"
                  value={userData.frequencytime}
                  onChange={(newValue) => {
                    setfields(newValue, "frequencytime");
                  }}
                  renderInput={(params) => (
                    <TextField
                      sx={{ width: "30%" }}
                      
                      helperText={errrormsg.frequencytime}
                      {...params}
                      variant="filled"
                    />
                  )}
                />
              </LocalizationProvider>
            </div>
          </div>
          <div className="section3">
            <TextFieldComponent
              errorMsg={userData.increamentTime}
              errorshow={errorshow}
              errorcontent={errrormsg.increamentTime}
              handleChange={handleChange}
              name="date"
              label="Increament Time"
              type="number"
              name="increamentTime"
            />
            <TextFieldComponent
              errorMsg={userData.frequency}
              errorshow={errorshow}
              errorcontent={errrormsg.frequency}
              handleChange={handleChange}
              name="date"
              label="Notification Frequency"
              type="number"
              name="frequency"
            />
          </div>
          <div className="section4">
            <RadioButtonComponent
              errorMsg={userData.copy}
              errorcontent={errrormsg.copy}
              handleChange={handleChange}
                title={"copy paramater"}
                name="copy"
                types={copyParameter}
            />
          </div>
          <div>
          <Button
            variant="contained"
            onClick={submitForm}
            sx={{ width: "20px", mt: "10px",float:"right"  }}
          >
            Save
          </Button>
          <Button
            onClick={goBack}
            variant="contained"
            sx={{ width: "20px", mt: "10px",mr:"10px",float:"right" }}
          >
            Cancel
          </Button>
          </div>
        </div>
        <ModalComponent
          handleOpen={handleOpen}
          setData={setModalData}
          data={userData.data[0]} 
          handleClose={handleClose}
          open={open}
        />
      </Box>
    </div>
  );
};
