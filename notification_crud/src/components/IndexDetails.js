import { Route, Switch } from "react-router-dom";
import { formRouter } from "../router/FormRouters";

export const IndexDetails=(props)=>{
    return(
        <div className="mainContainer">
              <Switch>
              {formRouter.map((item) => (
                <Route
                  key={item.path}
                  path={item.path}  
                  exact={item.exact}
                  component={item.component}
                />
              ))}
            </Switch>
      
          </div>
        );
      

}