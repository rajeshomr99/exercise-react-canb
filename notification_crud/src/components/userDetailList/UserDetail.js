import { Button } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { editDetails, updateDetails } from "../../reduxHandler/action/FormAction";
import { FormPath } from "../../router/routerPath";
import { TableList } from "./exampleTable";
import "./userDetails.scss";

export const UserDetail=(props)=>{
    const formvalue = useSelector(state => state);
    const dispatch = useDispatch();
    const [formDetail, setForm] = useState(formvalue.userDetails);
    useEffect(() => {
        dispatch(editDetails(-1))
    }, [])
    const createForm=()=>{

        props.history.push(FormPath);
    }
    const deleteitem=(index)=>{
        formDetail.splice(index,1);
        setForm([...formDetail]);
        dispatch(updateDetails(formDetail))
    }
    const editItem=(index)=>{
        dispatch(editDetails(index))
        props.history.push(FormPath);
    }
    return(
        <>
        <div className="tablelistHead">
            <div className="title"><b>Notification</b></div>
            <div class="newUser"><Button onClick={createForm} variant="contained" color="success">New</Button></div>
        </div>
        <div className="tablelist">
        <TableList details={formDetail} editItem={editItem} deleteitem={deleteitem}/>
        </div>
        </>
    );
}