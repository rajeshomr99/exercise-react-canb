
import { useMemo, useState } from "react";
import { useSortBy, useTable } from "react-table";
import "./tableList.scss";

export const TableList=(props)=>{
    const{details,deleteitem,editItem}=props;
    const [id, setmodal] = useState({no:"",check:false});
    const openModal=(value,checkvalue)=>{
      
    if(id.no===value){
      setmodal({...id,no:value,check:checkvalue?false:true});

    }
    else{
      setmodal({...id,no:value,check:true});
    }
    }

    function Table({ columns, data }) {
      
        const {getTableProps,getTableBodyProps,headerGroups,rows,prepareRow} = useTable({columns,data},useSortBy);
        return (
          <table {...getTableProps()}>
             
            <thead>
              {headerGroups.map(headerGroup => (
              <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  {column.render('Header')}
                  {column.id !=="edit" &&
                  <span className="sortingSymbol">
                    {column.isSorted
                      ? column.isSortedDesc
                        ?        <i class="fas fa-sort-down"></i>
                        : <i class="fas fa-sort-up"></i>
                      : <i class="fas fa-sort-down"></i>
                    }
                  </span>}
                  
                </th>
              ))}
            </tr>
              ))}
            </thead>
            <tbody {...getTableBodyProps()}>
              {rows.map((row, i) => {

                prepareRow(row)
                return (
                  <tr {...row.getRowProps()}>
                    {row.cells.map((cell ,index)=> {
                      return <td {...cell.getCellProps()}>{cell.render('Cell')}
                      
                       {cell.column.id==="edit" &&
                      <>
                      <span className="edit"><i class="fas fa-ellipsis-v" onClick={(e)=>openModal(row.original.id,id.check)}  ></i></span>
                     {(id.no===row.original.id && id.check=== true) &&
                      <div className="editDelete">
                        <div className='edittable' onClick={(e)=>editItem(row.original.id)}>Edit</div>
                        <div className='deleteTable' onClick={(e)=>deleteitem(i)}>delete</div>
                      </div>
                    }

                      </>
                      } 
                      </td>
                    })}
                  </tr>
                )
              })}
            </tbody>
          </table>
        )
      }
      const columns = useMemo(
        () => [
          {
            Header: 'ProductName',
            accessor: 'productName'  
          },
          {
            Header: 'Subject',
            accessor: 'subject'  
          },
          {
            Header: 'Notification Type',
            accessor: 'notificsationType'  
          },
          {
            Header: 'Content',
            accessor: 'content'  
          },
          {
            Header: 'Action',
            accessor: 'edit'  
          },
        ],
        []
      )
    
      const data =useMemo(() => [...details], [details])
    return(
        <>
         <Table columns={columns} data={data} />
        </>
    );
} 