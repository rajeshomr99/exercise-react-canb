import { FormControl, FormHelperText, InputLabel, MenuItem, Select } from "@mui/material";
import { red } from "@mui/material/colors";
export const SelectFieldComponent=(props)=>{
    const {handleChange,errorMsg,errorshow,errorcontent,options}=props;
    return(
        <FormControl variant="filled" sx={{ m: 1}}>
        <InputLabel id="demo-simple-select-filled-label" >Product Name</InputLabel>
        <Select value={errorMsg}   error={(errorMsg===""&&errorshow)} onChange={(e)=>handleChange(e,"productName")}
          labelId="demo-simple-select-filled-label"
          id="demo-simple-select-filled"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {options.map((item)=>(
            <MenuItem value={item.name}>{item.name}</MenuItem>
          ))}  
        </Select>
        <FormHelperText sx={{color:"red"}}>{errorcontent}</FormHelperText>
      </FormControl>
    );
}