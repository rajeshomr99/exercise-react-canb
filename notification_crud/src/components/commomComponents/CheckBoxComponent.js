import { Checkbox, FormControlLabel, FormGroup } from "@mui/material";

export const CheckBoxComponent=(props)=>{
    const{content,checkedData,setcheckedData,name}=props;
    return(
        <>
        {content.map((item,i)=>
         <FormGroup key={i}>
         <FormControlLabel control={<Checkbox checked={checkedData.some(sc=>sc ===item.id)} onChange={(e)=>setcheckedData(item.id,name)} />} label={item.name} />
       </FormGroup>)}
       
      </>
    );
}