import { Autocomplete, Chip, TextField } from "@mui/material";

export const MultiVale=(props)=>{
    const {label,optionList,value,errorcontent,errorshow,errorMsg,addmutiplevalue}=props;
    return (
        
      <Autocomplete
      multiple
      value={value}
      id="fixed-tags-demo"
      onChange={(event, newValue) => {
        addmutiplevalue(event,newValue,label);
      }}
      options={optionList}
      getOptionLabel={(option) => option.title}
      renderTags={(tagValue, getTagProps) =>
        tagValue.map((option, index) => (
          <Chip
            label={option.title}
            {...getTagProps({ index })}
          
          />
        ))
      }
      renderInput={(params) => (
        <TextField {... params} label={label} helperText={errorcontent} error={(errorMsg.length ===0 &&errorshow)} variant="filled"  />
      )}
    />
    );
}