import { FormControl, FormControlLabel, FormHelperText, FormLabel, Radio, RadioGroup } from "@mui/material";

export const RadioButtonComponent=(props)=>{
    const{handleChange,errorMsg,errorcontent,types,title,name}=props;
    return(
        <FormControl component="fieldset" id='notificationRadio'>
      <FormLabel component="legend"> {title}</FormLabel>
      <RadioGroup  onChange={(e)=>handleChange(e,name)} row aria-label="notificationType" name="row-radio-buttons-group">
        {types.map((item)=>(
            <FormControlLabel value={item.id} control={<Radio size="small" checked={errorMsg==item.id} />} label={item.name} />
        ))}
      </RadioGroup>
      <FormHelperText sx={{color:"red",mt:"-5px"}}>{errorcontent}</FormHelperText>
    </FormControl>
    );
}