import { TextField } from "@mui/material";

export const TextFieldComponent = (props) => {
  const { handleChange, type, name, label, errorshow, errorcontent, errorMsg } =
    props;
  return (
    <TextField  
      id={name}
      helperText={errorcontent}
      value={errorMsg}
      inputProps={{ maxLength: 20 }}
      type={type}
      error={(errorMsg === "" && errorshow) || errorcontent.length > 0}
      label={label}
      sx={{ m: 1 }}
      onChange={(e) => handleChange(e, name)}
      variant="filled"
    />
  );
};
