import './App.scss';
import { IndexDetails } from './components/IndexDetails';

function App() {
  return (
    <div className="App">
      <IndexDetails />    
    </div>
  );
}

export default App;
