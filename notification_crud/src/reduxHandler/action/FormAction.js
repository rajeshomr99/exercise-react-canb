import { Add, Edit } from "./actionType";

export const addDetails = (val) => {
    return (dispatch, getState) => {
        let {userDetails}=getState();
        userDetails.push(val);
        dispatch({type:Add,payload:userDetails});
    };
  };
  export const updateDetails = (val) => {
    return (dispatch, getState) => {
      dispatch({type:Add,payload:val});
    };
  };
  export const editDetails = (val) => {
    return (dispatch, getState) => {
      dispatch({type:Edit,payload:val});  
    };
  };
